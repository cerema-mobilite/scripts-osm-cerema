#! /usr/bin/env python
# -*- coding: utf-8 -*-
# P Gendre cete med 17/06/15
# transformation de l'export des arrêts osm : arrets_osm_210415.csv
# en fichier conforme aux champs de la base ATC

def main():
    import csv
    st=[]
    f=open('quays_osm_210415.csv','w')
    with open('arrets_osm_210415.csv', 'r') as csvfile:
        r = csv.reader(csvfile, delimiter='|')
        r.next() # on passe le titre
        f.write("id|name|latitude|longitude|srsname|postalreg_|siteref|transpmod_|gml_pos|street|wheelchac_|privatcode_|publiccode|descripti_|tariffzon_|url|fournisse_|dern_maj|nbhab_300m|min_time|max_time|amplitude|nbpas_intr|nbpas_pnte|nbpas_crse|nbpas_tot")
        for row in r:
            quay=[""]*26  # initialisation des 26 champs du modèle ATC quays
            st.append(row[0])
            # pour lister les valeurs possibles d'un champ (ex. le 6ème) et compter le nb d'occurrences
            #  awk -F"|" '{ print $6}' arrets_osm_210415.csv | sort | uniq -c
           
            # champ 6 highway : seule valeur possible bus_stop pour renseigner le mode, les autres valeurs ne sont pas prises en compte ()
            if row[5]=="bus_stop":
                quay[7]="bus"
            # champ 7 railway : valeur possible tram_stop pour renseigner le mode tram, les autres valeurs ne sont pas prises en compte (buffer_stop crossing halt platform railway station stop tram_stop yes)
            # station ou platform n'est pas utilisé car ne discrimine pas entre train, tram, métro
            if row[6]=="tram_stop":
                quay[7]="bus"
            # champ 8 public_transport : valeur possible bus bus_stop pour renseigner le mode bus, station pour le train, les autres valeurs ne sont pas prises en compte (board bus bus_stop plateform platform platform;stop_position public_transport station stop stop area stop_area stop position stop_position)
            # On ELIMINE LES VALEURS "stop area" et "stop_area" (seulement 30 sur 100000)
            if row[7] in ("bus","bus_stop"):
                quay[7]="bus"
            if row[7] in ("stop area","stop_area"):
                continue                
            # champ 9 bus : valeur possible yes oui bus pour renseigner le mode bus, les autres valeurs ne sont pas prises en compte (bus no on_demand oui true yes yes;no)
            if row[8] in ("yes","oui","bus"):
                quay[7]="bus"
            # champ 10 subway : valeur possible yes subway pour renseigner le mode subway
            if row[9] in ("yes","subway"):
                quay[7]="subway"
            # champ 11 train : valeur possible yes train pour renseigner le mode train
            if row[10] in ("yes","train"):
                quay[7]="train"
            # champ 12 station : valeur possible subway pour renseigner le mode subway
            if row[11] == "subway":
                quay[7]="subway"
            # IL FAUDRAIT ELIMINER LES VALEURS "disused" et "construction" (seulement 5 sur 100000)
            if row[11] in ("disused","construction"):
                continue
            # champ 13 network : utilisé pour décrire une ligne ou un réseau de TC
            # champ 14 operator : utilisé parfois pour décrire l'AO, le réseau, l'opérateur TC
            # champ 16 wikipedia 
            # on pourrait utiliser ces champs pour le champ description de quays
            # dans la version précédente, on mettant le contenu de tous les champs sauf ceux utilisés pour les modes
            # champ 15 wheelchair : valeur possible yes wheelchair pour renseigner le champ wheel_chair_access
            # il y a une valeur "limited" qu'on pourrait utiliser?
            if row[14] in ("yes","wheelchair"):
                quay[10]="yes"
            if row[14] == "no":
                quay[10]="no"
            # on pourrait utiliser le champ id pour renseigner aussi l'url openstreetmap.org/node/id
            quay[0]=row[0] #id
            quay[1]=row[4] #name
            quay[2]=row[2] #lat
            quay[3]=row[1] #lon
            quay[4]="EPSG:4326" #srs
            quay[17]=row[3] #dernière mise à jour
                
            f.write('|'.join(quay))
            f.write('\n')
    f.close()

if __name__ == '__main__':
    main()