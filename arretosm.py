#! /usr/bin/env python
# -*- coding: utf-8 -*-
# P Gendre cete med 17/06/15
# fusion des 2 fichiers : sur les 54000 arrêts de pt_stops.csv, 17000 environ ne sont pas dans stops.csv
# on les ajoute dans le fichier fusionné arrets_osm_210415.csv, on enlève les guillemets et convertit les champs en minuscules
# (QGIS n'accepte pas les " dans les champs donc on les a supprimés)

def main():
    import csv
    st=[]
    f=open('arrets_osm_210415.csv','w')
    with open('stops2104.csv', 'r') as csvfile:
        r = csv.reader(csvfile, delimiter='|')
        for row in r:
            st.append(row[0])
            f.write(('|'.join(row)).replace('"','').lower())
            f.write('\n')
    with open('pt_stops2104.csv', 'r') as csvfile:
        r = csv.reader(csvfile, delimiter='|')
        for row in r:
            id=row[0]
            if id not in st :
                # pour vérifier les valeurs possibles d'un champ (ex. le 6ème)
                #  awk -F"|" '{ print $6}' arrets_osm_210415.csv | sort -u
                # produit la liste des valeurs uniques de ce champ
                # champ 6 highway : valeurs possibles bus_stop ou , les autres valeurs ne sont pas prises en compte
                f.write(('|'.join(row)).replace('"','').lower())
                f.write('\n')
    f.close()


if __name__ == '__main__':
    main()