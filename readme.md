Extraction de données Transport d'OpenStreetMap pour la France
==============================================================

Ce site gitlab décrit des scripts d'extraction de données transport pour toute la France à partir de la base de données OSM.
Il est [documenté ici](https://gitlab.com/cerema-mobilite/scripts-osm-cerema/wikis/home)

Cf. aussi https://mim.cete-aix.fr/spip.php?article358 : 
scripts développés en 2015 par Frédéric Rodrigo pour le CEREMA  
[Licence](https://mim.cete-aix.fr/IMG/pdf/notices-rediffusion.pdf) (format pdf - 526.4 ko - 06/12/2013)   
[Scripts d’extraction des données](https://mim.cete-aix.fr/IMG/bz2/scripts-v1.3.tar.bz2) (format bz2 - 10.6 ko - 06/12/2013)   
[Description des données OSM extraites](https://mim.cete-aix.fr/IMG/pdf/notices-donnees-v1.1.pdf) (format pdf - 111 ko - 09/12/2013)   
[Présentation transport OSM @SOTM14](https://mim.cete-aix.fr/IMG/pdf/couches-tc-sotm14.pdf) (format pdf - 1.5 Mo - 07/04/2014)    